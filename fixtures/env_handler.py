import os

from dotenv import load_dotenv


def load_secrets():

    # Verifica se o arquivo .env existe
    if os.path.exists(".env"):
        load_dotenv(".env")  # Carrega as variáveis de ambiente do arquivo .env

    # Cria variaveis com base no valor das variáveis de ambiente
    try:
        url = os.environ['URL']
        user = os.environ['USER']
        password = os.environ['PASSWORD']
    except KeyError as e:
        print(f'ERROR: Falha ao obter valor da variável de ambiente: {e}')
        print('Certifique-se de criar as variáveis de ambiente URL, USER e PASSWORD no Sistema Operacional ou em um arquivo .env com valores válidos.')
        exit(-1)
    except Exception as e:
        print(f'ERROR: Ocorreu um erro inesperado ao obter variáveis de ambiente: {e}')
        exit(-1)

    if not all([url, user, password]):
        raise ValueError("ERROR: Falha ao obter valor da variável de ambiente")

    return url, user, password