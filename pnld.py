import time

from playwright.sync_api import sync_playwright
import pytest
from pytest_bdd import scenario, given, when, then
from fixtures import env_handler
from pages.page_login import PageLogin

# Carregar as variáveis de ambiente
url, user, password = env_handler.load_secrets()

@pytest.fixture(scope='session')
def browser():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=False)
        yield browser
        browser.close()


@scenario('features/login.feature', 'Login bem-sucedido')
def test_successful_login():
    print("Sucesso!")
    pass


@pytest.fixture(scope='function')
def page_login(browser):

    page = browser.new_page()
    page_login = PageLogin(page)
    return page_login


@given('que estou na página de login')
def given_login_page(page_login):
    
    page_login.go_to(url)
    page_login.click_button_enter()

# When steps
@when('eu insiro credenciais válidas')
def when_enter_valid_credentials(page_login):
    page_login.fill_input_username(user)
    page_login.click_button_continue()
    page_login.fill_input_password(password)


@when('eu clico no botão de login')
def when_click_login_button(page_login):
    page_login.click_button_submit()


# Then step
@then('eu devo ser redirecionado para a página principal do PNLD')
def then_verify_redirected_to_dashboard(page_login):
    page_login.page.wait_for_url('https://pnld-formacao-hmg.nees.ufal.br/ead/formacao')
    
    time.sleep(5)

    # Assert
    assert True