class PageLogin:


    def __init__(self, page):
        self.page = page
        
        # componentes
        self.button_enter = r'#login-entrar-gov'
        self.input_user = r'#cpf'
        self.input_password = r'#password'
        self.button_continue = r'//*[@id="__next"]/div/div[2]/div/div/div/form/div/button'
        self.button_submit = r'//*[@id="__next"]/div/div[2]/div/div/div/form/div/button'
        self.header_h1 = r'//*[@id="__next"]/div/div[2]/div[2]/div/div[1]/div[2]/div/h1'
   
    def go_to(self, url):
        self.page.goto(url)

    def fill_input_username(self, name):
        self.page.fill(self.input_user, name)
       
        
    def fill_input_password(self, password):
        self.page.fill(self.input_password, password)

    def click_button_enter(self):
        self.page.click(self.button_enter)

    def click_button_continue(self):
        self.page.click(self.button_continue)   
        
    def click_button_submit(self):
        self.page.click(self.button_submit)

    