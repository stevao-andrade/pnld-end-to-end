Feature: Login bem sucedido
    Como um usuário
    Eu quero ser capaz de fazer login na minha conta
    Para que eu possa acessar recursos restritos

    Scenario: Login bem-sucedido
        Given que estou na página de login
        When eu insiro credenciais válidas
        When eu clico no botão de login
        Then eu devo ser redirecionado para a página principal do PNLD
