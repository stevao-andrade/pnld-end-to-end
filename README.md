# Produção Textual End To End

## Ambiente Virtual

Para configurar um ambiente virtual, execute o seguinte comando:

```bash
python -m venv .venv
```

## Ativar o Ambiente

Ative o ambiente virtual com o comando adequado ao seu sistema operacional:

- **Windows:**

    ```bash
    .venv\Scripts\activate
    ```

- **Linux/Mac:**

    ```bash
    source .venv/bin/activate
    ```

## Instalar Dependências do Projeto

Instale as dependências do projeto utilizando o seguinte comando:

```bash
pip install -r requirements.txt
```

## Instalar Drivers de Navegadores

Instale os drivers dos navegadores necessários com o comando:

```bash
playwright install
```

## Configuração das Variáveis de Ambiente

Para configurar as variáveis de ambiente ou utilizar um arquivo .env, siga os passos abaixo:

Crie um arquivo .env na raiz do projeto:

```bash
URL=url_valida
USER=usuario_valido
PASSWORD=senha_valida
```

As variáveis de ambiente serão carregadas automaticamente do arquivo .env, ou do Sistema Operacional, caso tenham sido definidas diretamente. 


## Executar Testes

Para executar os testes, utilize o seguinte comando:

```bash
pytest -s
```

O parâmetro `-s` exibe os prints presentes no código durante a execução dos testes.

## Relatório dos Testes

Para visualizar um relatório dos testes, utilize o seguinte comando:

```bash
allure serve allure-results
```

Certifique-se de ter o Allure instalado no seu sistema para gerar e visualizar relatórios. Caso não tenha, siga as instruções em [Allure Framework](https://docs.qameta.io/allure/).